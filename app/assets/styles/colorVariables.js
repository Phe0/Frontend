const colors = {
  primary: "#4b8ab9",
  secondary: "#f7ef6e",
  danger: "#e47171",
  dark: "#353535",
  light: "#f7f7f7"
};

export default colors;
