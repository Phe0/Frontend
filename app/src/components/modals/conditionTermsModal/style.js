import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  closeButton: {
    position: "absolute",
    zIndex: 5,
    right: 15,
    top: 15,
  },
  webView: { flex: 1 },
});
export default styles;
